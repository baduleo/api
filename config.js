// migrate-mongo configuration
const host = process.env.DB_HOST || 'localhost:27017'
const url = `mongodb://${host}`

module.exports = {
  mongodb: {
    url,
    databaseName: process.env.DB_DB || 'baduleo',
  },
  migrationsDir: 'migrations',
  changelogCollectionName: 'changelog',
}
