Baduleo API
===========

[![build][]][build link]
[![coverage][]][coverage link]

Development
-----------

Use jest to test your changes:

~~~bash
yarn test
~~~

You can also check for coverage:

~~~bash
yarn test --coverage
~~~

Or check coverage & open browser with results:

~~~bash
yarn run coverage
~~~

### Configuration

Currently available environment variables:

- `DB_DB`: Sets the database name.
- `DB_HOST`: Sets the db host(s) and port, if set.
- `DB_URL`: Sets the entire mongodb:// url (not required if DB and/or HOST are set).

For development, you can set these vars in an `.env` file in the root path of this project, which could look like:

~~~ini
DB_HOST=localhost,otherhost,eventhirdhost:8956
DB_DB=baduleo-is-awesome
~~~

> Note: test environment are set using a `.env.test` file instead.

### Migrations

[`migrate-mongo`][migrate-mongo] has been added for migrations. To use it, access it with `yarn run`:

~~~bash
yarn run migrations
~~~

An alias of `yarn run migrations up` has been created for easier usage:

~~~bash
yarn run migrate
~~~


[coverage]: https://gitlab.com/baduleo/api/badges/master/coverage.svg
[build]: https://gitlab.com/baduleo/api/badges/master/build.svg

[coverage link]: https://gitlab.com/baduleo/api/-/jobs
[build link]: https://gitlab.com/baduleo/api/pipelines
