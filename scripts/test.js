import dotenv from 'dotenv'

// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'test'
process.env.NODE_ENV = 'test'
process.env.PUBLIC_URL = ''
// Load .env files
dotenv.config({
  path: '.env.test',
})
process.env.PORT = process.env.PORT || 8951
process.env.DB_HOST = process.env.DB_HOST || 'localhost:27017'
process.env.DB_DB = process.env.DB_DB || 'test'

const { DB_HOST, DB_DB } = process.env

process.env.DB_URL = `mongodb://${DB_HOST}/${DB_DB}`

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err
})

const jest = require('jest')
const argv = process.argv.slice(2)

// Watch unless on CI or in coverage mode
if (!process.env.CI && argv.indexOf('--coverage') < 0) {
  argv.push('--watch')
}
// I've been trying to close the mongoose connection without success...
// so here's the result
if (process.env.CI || argv.indexOf('--coverage') >= 0) {
  argv.push('--forceExit')
}

jest.run(argv)
