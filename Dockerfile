FROM node:8

LABEL maintainer="Òscar Casajuana <elboletaire@underave.net>"

COPY . /app-src

WORKDIR /app-src

RUN yarn && yarn run build && \
    mkdir /app && \
    mv -f entrypoint.sh package.json migrations config.js build/* /app

WORKDIR /app

RUN rm -fr /app-src && \
    yarn

ENTRYPOINT ["/app/entrypoint.sh"]
