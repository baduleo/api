/*
 * API configuration constants
 */
const { HOST, PORT, PREFIX, DB_DB, DB_HOST, DB_PORT, DB_URL } = process.env

const appPort = PORT || 8080
const prefix = PREFIX || 'api'
let appHost = HOST || 'http://localhost'

if (parseInt(appPort, 10) !== 80) {
  appHost += `:${appPort}`
}

if (prefix) {
  appHost += `/${prefix}`
}

export const api = {
  prefix,
  host: appHost,
  port: appPort,
}

const port = DB_PORT || 27017
const name = DB_DB || 'baduleo'
const host = DB_HOST || 'localhost'
const url = DB_URL || `mongodb://${host}:${port}/${name}`

export const database = {
  name,
  port,
  host,
  url,
}
