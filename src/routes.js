import { Router } from 'express'
import bodyParser from 'body-parser'

import Badu from './models/badu'
import { api } from './constants'

const routes = (app) => {
  const router = Router()

  router.get('/', (req, res) => {
    res.status(200).send({text: 'Hello world 😃'})
  })

  router.get('/badus', (req, res) => {
    Badu.find().then((result) => {
      res.status(200).send(result)
    })
  })

  router.get('/badus/:id', (req, res) => {
    Badu.findById(req.params.id).then((result) => {
      res.status(200).send(result)
    })
  })

  router.post('/badus', (req, res) => {
    const badu = new Badu(req.body)

    badu.save((error) => {
      if (!error) {
        return res.status(201).send(badu)
      }

      return res.status(400).send(error.errors)
    })
  })

  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())

  app.use(`/${api.prefix}`, router)
}

export default routes
