import uuid from 'uuid/v4'

import mongoose from '.'

export const BaduSchema = mongoose.Schema({
  _id: {
    type: String,
    default: uuid,
  },
  name: {
    type: String,
    required: true,
  },
  from: {
    type: String,
    required: true,
  },
  to: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  lat: {
    type: Number,
    required: true,
  },
  lng: {
    type: Number,
    required: true,
  },
  deletedAt: {
    type: Date,
  },
}, {
  // Enable updatedAt & createdAt fields
  timestamps: true,
})

BaduSchema.virtual('id').get(function() {
  return this._id
})

BaduSchema.set('toJSON', {
  virtuals: true,
})

const Badu = mongoose.model('Badu', BaduSchema)

export default Badu
