import mongoose from 'mongoose'
import logger from '../logger'

import { database } from '../constants'

logger.info(`Connecting to db ${database.url}`)
mongoose.connect(database.url).catch((error) => {
  logger.error('MongoDB connection error', error)
})

export const connection = mongoose.connection

export default mongoose
