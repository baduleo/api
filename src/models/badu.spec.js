import Badu from './badu'

const model = {
  name: 'test',
  from: '08:00:00',
  to: '16:30:00',
  address: 'testing address',
  lat: 41.4208330225,
  lng: 2.160349745,
}

describe('models::Badu', () => {
  it('should generate a uuid id', () => {
    const badu = new Badu(model)

    badu.save().then((badu) => {
      expect(badu.id).toMatch(/[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}/)
    })
  })
  it('should be invalid with empty fields', () => {
    const badu = new Badu()

    badu.validate().catch(err => {
      expect(err.errors.name).toBeDefined()
    })
  })
})
