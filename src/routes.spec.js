import express from 'express'
import request from 'supertest'
import fixtures from 'node-mongoose-fixtures'

import badus from './models/fixtures/badu.json'
import Badu from './models/badu'
import routes from './routes'

const app = express()

beforeAll(() => {
  routes(app)
})

describe('routes', () => {
  describe('GET /', () => {
    it('returns 200', (done) => {
      request(app).get('/api').then((response) => {
        expect(response.statusCode).toBe(200)
        done()
      })
    })
    it('returns hello world text', (done) => {
      request(app).get('/api').then((response) => {
        expect(response.body.text).toBe('Hello world 😃')
        done()
      })
    })
  })

  describe('GET /badus', () => {
    // Remove & load fixtures
    beforeEach((done) =>
      Badu.remove().then(() => fixtures({Badu: badus}, done))
    )

    it('returns 200', (done) => {
      request(app).get('/api/badus').then((response) => {
        expect(response.statusCode).toBe(200)
        done()
      })
    })
    it('returns an array of badus', (done) => {
      request(app).get('/api/badus').then((response) => {
        expect(response.body).toBeInstanceOf(Array)
        done()
      })
    })
    it('each item of the array has the virtual field "id" defined', (done) => {
      request(app).get('/api/badus').then((response) => {
        response.body.forEach((badu) => {
          expect(badu.id).toBeDefined()
        })
        done()
      })
    })
    it('returns exactly the amount of badus we have in db', (done) => {
      request(app).get('/api/badus').then((response) => {
        expect(response.body.length).toBe(2)
        done()
      })
    })
  })

  describe('GET /badus/:id', () => {
    // Remove & load fixtures
    beforeEach((done) =>
      Badu.remove().then(() => fixtures({Badu: badus}, done))
    )

    it('returns 200', (done) => {
      request(app).get('/api/badus/0e2d9778-c2e5-4345-a32b-888a0a1fea38')
        .then((response) => {
          expect(response.statusCode).toBe(200)
          done()
        })
    })
    it('returns an object with the badu', (done) => {
      request(app).get('/api/badus/0e2d9778-c2e5-4345-a32b-888a0a1fea38')
        .then((response) => {
          expect(response.body).toBeInstanceOf(Object)
          done()
        })
    })
    it('should set the virtual field "id"', (done) => {
      request(app).get('/api/badus/0e2d9778-c2e5-4345-a32b-888a0a1fea38')
        .then((response) => {
          expect(response.body.id).toBeDefined()
          // expect(response.body.id).toBeInstanceOf(String)
          done()
        })
    })
  })
  describe('POST /badus', () => {
    // Remove & load fixtures
    beforeEach((done) =>
      Badu.remove().then(() => fixtures({Badu: badus}, done))
    )

    const badu =  {
      name: 'test',
      from: '08:00:00',
      to: '16:30:00',
      address: 'testing address',
      lat: 41.4208330225,
      lng: 2.160349745,
    }

    it('returns 201 on valid post', (done) => {
      request(app).post('/api/badus')
        .send(badu)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.statusCode).toBe(201)
          done()
        })
    })
    it('returns 400 on invalid post, plus proper validation error', (done) => {
      const invalid = {...badu}
      delete invalid.name
      request(app).post('/api/badus')
        .send(invalid)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.statusCode).toBe(400)
          expect(response.body.name).toBeInstanceOf(Object)
          done()
        })
    })
  })
})
