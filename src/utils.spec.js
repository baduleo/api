import { ucfirst } from './utils'

describe('utils', () => {
  it('::ucfirst', () => {
    expect(ucfirst('testing')).toEqual('Testing')
    expect(ucfirst('àustria')).toEqual('Àustria')
  })
})
