import dotenv from 'dotenv'
import cors from 'cors'
import express from 'express'

import logger from './logger'
import routes from './routes'
import { api } from './constants'

const app = express()

// Load .env file (if any)
dotenv.config()

logger.info('Initialising 🚧')
// Some middlewares are not required under testing, that's why they stay here
app.use(cors({
  preflightContinue: true,
}))
// Load routes and other usefull middlewares
routes(app)

app.listen(api.port, () => {
  logger.info(`Baduleo API 🏃 at ${api.host}️`)
})

export default app
