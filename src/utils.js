/**
 * Ensures a string has its first
 * character as uppercase.
 *
 * @param {string} string String to be converted.
 */
export const ucfirst = (string) =>
  string.substr(0, 1).toUpperCase() + string.substr(1)
