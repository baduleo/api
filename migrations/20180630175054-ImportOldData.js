const uuid = require('uuid/v4')
const badus = require('./badus.json')

module.exports = {
  up(db, next) {
    badus.forEach((badu) => {
      const store = {
        // New unique uuidv4 for everyone
        _id: uuid(),
        name: badu.name,
        from: badu.from_hour,
        to: badu.to_hour,
        address: badu.address,
        lat: badu.lat,
        lng: badu.long,
        deletedAt: badu.deleted_at,
        createdAt: badu.created_at,
        updatedAt: badu.updated_at,
      }

      db.collection('badus').insert(store)
    })
    next()
  },
  down(db, next) {
    db.collection('badus').remove({})
    next()
  },
}
